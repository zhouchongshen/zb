import { createRouter, createWebHashHistory } from "vue-router";

import Login from "../views/Login.vue";
import HomePage from "../views/HomePage.vue";
import NewBlog from "../components/NewBlog.vue";
import NotFound from "../components/NotFound.vue";
import Category from "../components/Category.vue";
import Blogs from "../components/Articles.vue";
import Welcome from "../components/Welcome.vue";
const routes = [
  { path: "/login", component: Login },
  {
    path: "/",
    redirect: "/home",
    children: [
      {
        path: "/home",
        component: HomePage,
        children: [
          { path: "newblog", component: NewBlog },
          { path: "category", component: Category },
          { path: "blogs", component: Blogs },
          { path: "welcome", component: Welcome },
        ],
      },
    ],
  },

  // 将匹配所有内容并将其放在 `$route.params.pathMatch` 下
  { path: "/:pathMatch(.*)*", name: "NotFound", component: NotFound },
];

const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
});
export default router;
