import axios from "axios";

const server = axios.create({
  // TODO 正式版本不写这个
  baseURL: "http://localhost:8999",
  timeout: 5000,
});
// 新建文章
export function addArticle(data) {
  return server.post("/user/addAriticle", data);
}
// 获取所有文章
export function getAllArticles() {
  return server.get("/user/getAllArticles");
}

// 删除某个文章
export function deleteArticle(id) {
  return server.post(`/user/deleteArticle/${id}`);
}

// 获取分类列表
export function getCategorys(id) {
  return server.get("/user/getCategorys");
}

// 删除分类
export function deleteCategorys(id) {
  return server.post(`/user/deleteCategory/${id}`);
}

// 添加分类
export function addCategory(data) {
  console.log("====================================");
  console.log(data);
  console.log("====================================");
  return server.post(`/user/addCategory/`, data);
}

// TODO 响应路由守卫还没写呢
