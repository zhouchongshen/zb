/*
 Navicat Premium Data Transfer

 Source Server         : zblog
 Source Server Type    : MySQL
 Source Server Version : 80035 (8.0.35)
 Source Host           : localhost:3306
 Source Schema         : zBlog

 Target Server Type    : MySQL
 Target Server Version : 80035 (8.0.35)
 File Encoding         : 65001

 Date: 26/12/2023 18:04:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for articles
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `contentText` longtext,
  `contentHtml` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `createTime` varchar(255) NOT NULL COMMENT '时间码',
  `status` varchar(255) NOT NULL COMMENT 'normal正常  deleted已删除',
  `log` text,
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `coverImgUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of articles
-- ----------------------------
BEGIN;
INSERT INTO `articles` (`id`, `title`, `contentText`, `contentHtml`, `createTime`, `status`, `log`, `category`, `coverImgUrl`) VALUES (12, '一上班被领导叫进办公室说是调节矛盾', '我一头雾水，心想我跟别人都不说话哪来的矛盾？\n走进他办公室一看另一个前段时间刚转进来的同事也在。\n他说他觉得我在针对他。\n他最近刚从集团另一个子公司转到我们这，合同要明年才能正式转过来，所以出差报销都是以我的名义去报的，他说他把发票给我后我没非得等很久才肯把钱给他。\n我说我TM又不是财务，钱没下来难道我自己垫钱给你？\n他说对啊。\n那你爱找谁要找谁报去，我不管了，我说完起身要走。\n领导叫住我然后一直和稀泥。\n出门后他乜着我问上次的钱什么时候给他，我说等着吧', '<p><span style=\"color: rgb(85, 85, 85); background-color: rgb(255, 255, 255); font-size: 14px;\">我一头雾水，心想我跟别人都不说话哪来的矛盾？<br>走进他办公室一看另一个前段时间刚转进来的同事也在。<br>他说他觉得我在针对他。<br>他最近刚从集团另一个子公司转到我们这，合同要明年才能正式转过来，所以出差报销都是以我的名义去报的，他说他把发票给我后我没非得等很久才肯把钱给他。<br>我说我TM又不是财务，钱没下来难道我自己垫钱给你？<br>他说对啊。<br>那你爱找谁要找谁报去，我不管了，我说完起身要走。<br>领导叫住我然后一直和稀泥。<br>出门后他乜着我问上次的钱什么时候给他，我说等着吧</span><img src=\"https://tx-free-imgs2.acfun.cn/kimg/bs2/zt-image-host/ChYwOGY5ODRhYzRjMTBiOWFkOTE5NTA2EJjM1y8.png\" alt=\"\" data-href=\"\" style=\"\"/></p>', '1703579301550', 'normal', 'create at 1703579301558', 'guide', 'http://localhost:8999/1-1703579261968.avif');
COMMIT;

-- ----------------------------
-- Table structure for categorys
-- ----------------------------
DROP TABLE IF EXISTS `categorys`;
CREATE TABLE `categorys` (
  `id` int NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of categorys
-- ----------------------------
BEGIN;
INSERT INTO `categorys` (`id`, `categoryName`) VALUES (1, '生活记录');
INSERT INTO `categorys` (`id`, `categoryName`) VALUES (2, '技术');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
