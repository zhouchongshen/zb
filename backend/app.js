// 引入express模块
const express = require("express");
const articlesRouter = require("./routers/articlesApi");
const categorysApi = require("./routers/categorysApi");
const files = require("./routers/files");
const cors = require("cors");

const bodyParser = require("body-parser"); //express集成的模块，用来解析post参数
// 创建web服务器
const app = express();
app.use(bodyParser.urlencoded({ extended: false })); //parse application/x-www-form-urlencoded
app.use(bodyParser.json()); //parse application/json

app.use(cors());
// 处理get请求的路由
app.use("/user", articlesRouter);
app.use("/user", categorysApi);
app.use(files);

app.use(express.static("./public"));
// 启动服务器
app.listen(8999, () => {
  console.log("服务器在8999端口启动");
  console.log("http://localhost:8999/");
});
