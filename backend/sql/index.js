// 导入mysql模块
const mysql = require("mysql");
// 创建数据库连接对象
const db = mysql.createPool({
  host: "localhost", //主机地址
  user: "root", //数据库账号
  password: "123456", //密码
  database: "zBlog", //需要使用的数据库名
});
// 向外共享db数据库连接对象
module.exports = db;
