1.获取所有文章列表（get）
/allArticles

2.获取所有分类（get）
/categories

3.根据分类获取该分类下的文章（get）
/articles/:categoryname
参数 categoryname

4.获取单独的文章（get）
/article/:id

5.获取推荐阅读列表（get）
/articles/recommend

6.上传一篇文章(post)
/addarticle

7.修改文章（post）
/editarticle/
params:{
id:number
}

8.管理端登录（post）
/login
params:{
username:string,
password:string
}

{
id:1,
kind:"技术博客",
children:[
{id:1,
name:"JavaScript"},
{
id:2,
name:"C++"
}
]
}
