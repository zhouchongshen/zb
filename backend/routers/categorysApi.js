const express = require("express");
const content = require("../articles.json");
const db = require("../sql/index");

const router = express.Router();
// 获取所有分类
router.get("/getCategorys", (req, res) => {
  const sql = `SELECT * FROM categorys`;
  db.query(sql, (err, result) => {
    console.log("🚀 ~ file: router.js:87 ~ db.query ~ result:", result);
    if (err) {
      return res.status(500).json({ error: "查询失败！" });
    }

    res.send({
      status: 0,
      message: "获取分类成功！",
      data: result,
    });
  });
});

// 添加分类
router.post("/addCategory", (req, res) => {
  const sql = `INSERT INTO  categorys (categoryName) VALUES (?);`;
  db.query(sql, req.body.categoryName, (err, result) => {
    if (err) {
      // console.log(err);
      return res.status(500).json({ error: "新增分类失败！" });
    }

    res.send({
      status: 0,
      message: "新增分类成功！",
    });
  });
});

// 删除分类
router.post("/deleteCategory/:id", (req, res) => {
  let id = req.params.id;
  const sql = `DELETE FROM categorys WHERE id = ?;`;
  db.query(sql, id, (err, result) => {
    if (err) {
      // console.log(err);
      return res.status(500).json({ error: "删除分类失败！" });
    }

    res.send({
      status: 0,
      message: "删除分类成功！",
    });
  });
});

// 导出路由对象
module.exports = router;
