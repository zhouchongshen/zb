const express = require("express");
const content = require("../articles.json");
const db = require("../sql/index");

const router = express.Router();

// 获取所有文章
router.get("/getAllArticles", (req, res) => {
  const sql = `SELECT * FROM articles WHERE status = ?`;
  db.query(sql, "normal", (err, result) => {
    if (err) {
      console.log("查询文章出错");
      return res.status(500).json({ error: "查询出错" });
    }

    res.send({
      status: 0,
      message: "获取文章列表成功！",
      data: result,
    });
  });
});

// 获取分类下所有文章
router.get("/getArticlesByCategory/:category", (req, res) => {
  console.log(req.params);

  let category = req.params.category;

  const sql = `SELECT * FROM articles WHERE status = ? AND category = ?`;
  db.query(sql, ["normal", category], (err, result) => {
    if (err) {
      console.log("查询文章出错");
      return res.status(500).json({ error: "查询出错" });
    }

    res.send({
      status: 0,
      message: "获取分类文章列表成功！",
      data: result,
    });
  });
});

// 获取单个文章
router.get("/getArticle/:id", (req, res) => {
  let id = req.params.id;
  const sql = `SELECT * FROM articles WHERE id = ?`;
  db.query(sql, id, (err, result) => {
    if (err) {
      console.log("查询文章出错");
      return res.status(500).json({ error: "查询出错" });
    }
    if (result.length == 0) {
      return res.status(400).json({ error: "未查询到数据！" });
    }

    res.send({
      status: 0,
      message: "获取文章列表成功！",
      data: result[0],
    });
  });
});
// 新建文章
router.post("/addAriticle", (req, res) => {
  const {
    title,
    contentHtml,
    contentText,
    createTime,
    status,
    category,
    coverImgUrl,
  } = req.body;
  if (!title || !createTime || !category) {
    return res.status(400).json({ error: "参数错误" });
  }

  const sql = `INSERT INTO articles (title, contentText, contentHtml, createTime,status,log,category,coverImgUrl)
  VALUES (?,?,?,?,?,?,?,?);`;
  db.query(
    sql,
    [
      title,
      contentText,
      contentHtml,
      createTime,
      status,
      `create at ${Date.now()}`,
      category,
      coverImgUrl,
    ],
    (err, results) => {
      // 1. 执行 SQL 语句失败
      if (err) {
        console.log("err=>>", err);
        res.status(500).json({ error: "写入数据库失败" });
        return;
      } // 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
      res.send({
        status: 0,
        message: "新建文章成功！",
      });
    }
  );
});
// 删除文章
router.post("/deleteArticle/:id", (req, res) => {
  let id = req.params.id;
  const sql = `UPDATE  articles SET status='deleted' WHERE id = ?`;
  db.query(sql, id, (err, result) => {
    if (err) {
      console.log("删除失败！", err);
      return res.status(500).json({ error: "删除失败" });
    }
    res.send({
      status: 0,
      message: "删除文章成功  ",
    });
  });
});
// 导出路由对象
module.exports = router;
