/* 
上传图片的接口
*/
const express = require("express");
const multer = require("multer");
// 创建router对象
const router = express.Router();

const storage = multer.diskStorage({
  //保存路径
  destination: function (req, file, cb) {
    cb(null, "./public");
    //注意这里的文件路径,不是相对路径，直接填写从项目根路径开始写就行了
  },
  //保存在 destination 中的文件名
  filename: function (req, file, cb) {
    let type = file.originalname.replace(/.+\./, ".");
    let iamgeName = file.originalname.replace(/\.[^/.]+$/, "");
    cb(null, iamgeName + "-" + Date.now() + type);
  },
});
const upload = multer({ storage: storage });

router.post("/upload", upload.single("file"), function (req, res, next) {
  console.log(req.file);
  const sendData = {
    errno: 0, // 注意：值是数字，不能是字符串
    data: {
      url: "http://localhost:8999/" + req.file.filename, // 图片 src ，必须
    },
  };
  res.send(sendData);
  // TODO 上传失败的逻辑
});

module.exports = router;
