import axios from "axios";

const server = axios.create({
  // TODO 正式版本不写这个
  baseURL: "http://localhost:8999",
  timeout: 5000,
});

// 获取所有文章
export function getAllArticles() {
  return server.get("/user/getAllArticles");
}

// 获取单个文章
export function getArticleById(id) {
  return server.get(`/user/getArticle/${id}`);
}

// 获取分类下的所有文章
export function getArticlesByCategory(category) {
  return server.get(`/user/getArticlesByCategory/${category}`);
}

// 获取分类列表
export function getCategorys() {
  return server.get("/user/getCategorys");
}
